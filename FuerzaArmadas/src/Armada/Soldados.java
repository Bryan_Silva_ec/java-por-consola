/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Armada;

/**
 *
 * @author bryan
 */
public abstract class Soldados {
    protected String nombre;
    protected String Ciudad;
    protected int edad;

    public Soldados(String nombre, String Ciudad, int edad) {
        this.nombre = nombre;
        this.Ciudad = Ciudad;
        this.edad = edad;
    }

    protected String getNombre() {
        return nombre;
    }

    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }

    protected String getCiudad() {
        return Ciudad;
    }

    protected void setCiudad(String Ciudad) {
        this.Ciudad = Ciudad;
    }

   protected int getEdad() {
        return edad;
    }

    protected void setEdad(int edad) {
        this.edad = edad;
    }
    
    
}
