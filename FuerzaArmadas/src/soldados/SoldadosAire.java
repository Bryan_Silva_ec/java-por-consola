/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soldados;
import Armada.*;
import interfaces.*;
/**
 *
 * @author bryan
 */
public class SoldadosAire extends Soldados implements sueldo {
    
    private String avion;
    private int años_vuelo;

    public SoldadosAire(String avion, int años_vuelo, String nombre, String Ciudad, int edad) {
        super(nombre, Ciudad, edad);
        this.avion = avion;
        this.años_vuelo = años_vuelo;
    }
    
     public SoldadosAire(String entrenador,String avion, int años_vuelo, String nombre, String Ciudad, int edad) {
        super(nombre, Ciudad, edad);
        this.avion = avion;
        this.años_vuelo = años_vuelo;
    }

   

    public String getAvion() {
        return avion;
    }

    public void setAvion(String avion) {
        this.avion = avion;
    }

    public int getAños_vuelo() {
        return años_vuelo;
    }

    public void setAños_vuelo(int años_vuelo) {
        this.años_vuelo = años_vuelo;
    }
    
    
    public String cambio_avion(String avion, int años) 
    {
      String cambio;
      if(avion == "cualquiera" && años > 5) {
        
       cambio = "cambiar de avion";
    } else {
            cambio = "el avion aun resiste";
     }
      return cambio;
    }

    @Override
    public int aumento(int sueldo, int años) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int retiro(int edad, int años) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
